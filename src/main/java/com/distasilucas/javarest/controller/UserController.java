package com.distasilucas.javarest.controller;

import com.distasilucas.javarest.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    Map<UUID, User> users = new HashMap<>();

    @GetMapping
    public Map<UUID, User> getUsers(@RequestParam(value = "page", defaultValue = "0") int page,
                                 @RequestParam(value = "limit", defaultValue = "0", required = false) int limit) {
        return users;
    }

    @GetMapping(path = "/{id}",
                consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<User> getUser(@PathVariable UUID id) {
        if (users.containsKey(id)) {
            return new ResponseEntity<User>(users.get(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(path = "/add",
                 consumes = { MediaType.APPLICATION_JSON_VALUE })
    public void addUser(@RequestBody @Valid User user) {
        User u = new User(user.getFirstName(), user.getLastName(), user.getEmail());
        users.put(UUID.randomUUID(), u);
    }

    @PutMapping(path = "/{id}",
                consumes = { MediaType.APPLICATION_JSON_VALUE },
                produces = { MediaType.APPLICATION_JSON_VALUE })
    public User updateUser(@PathVariable(value = "id") UUID id, @RequestBody @Valid User user) {
        if (users.containsKey(id)) {
            users.replace(id, user);
            return users.get(id);
        } else {
            return null;
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable(value = "id") UUID id) {
        users.remove(id);
        return ResponseEntity.noContent().build();
    }
}
