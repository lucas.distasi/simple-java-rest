package com.distasilucas.javarest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class User {

    @NotNull (message = "First name can not be empty")
    @NotEmpty
    @Size(min = 2, max = 30)
    private String firstName;

    @NotNull (message = "Last name can not be empty")
    @NotEmpty
    @Size(min = 2, max = 30)
    private String lastName;

    @NotNull (message = "Email can not be empty")
    @Email
    @Size(min = 3, max = 30)
    private String email;
}
